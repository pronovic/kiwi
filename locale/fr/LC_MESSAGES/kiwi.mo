��    (      \  5   �      p  %   q  '   �  &   �     �  (         *     K     S     l     �     �     �     �  /   �     �     �  )   �  %        6     >     C     J  	   O     Y     a     f     y  5   �     �  :   �  $   	     .     3     :  ?   B  @   �     �     �     �  E  �  (      "   I  )   l     �  2   �  $   �     	  "   	  #   9	     ]	     d	     i	     w	  5   �	     �	     �	  (   �	  #   
     /
     6
     B
     H
     O
  	   ^
     h
     t
     �
  9   �
     �
  =   �
  +   (     T     Z     b  >   j  @   �     �     �          (             #                                                                	   
       !                '   $                    "         &      %                              %s can not be converted to a currency %s could not be converted to an integer '%s' can not be converted to a boolean '%s' is not a valid object '%s' is not a valid value for this field A file named "%s" already exists Boolean Could not load image: %s Could not open file "%s" Currency Date Date and Time Decimal Do you wish to replace it with the current one? Finish Float Inproperly placed thousand separators: %r Inproperly placed thousands separator Integer Long Object Open Password: Replace Save Show more _details String The file "%s" could not be opened. Permission denied. This field is mandatory This field requires a date of the format "%s" and not "%s" This field requires a number, not %r Time Total: Unicode You have a thousand separator to the right of the decimal point You have more than one decimal point ("%s")  in your number "%s" _Cancel _Select _Today Project-Id-Version: kiwi 1.9.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-05-28 17:38-0300
PO-Revision-Date: 2007-04-03 12:41-0300
Last-Translator: Benoit Myard <myardbenoit@gmail.com>
Language-Team: French <myardbenoit@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s n'a pu être converti vers une devise %s n'a pu être converti en entier '%s' n'a pu être converti en un booléen '%s' n'est pas un objet valide '%s' n'est pas une valeur autorisée pour ce champ Un fichier nommé "%s" existe déjà Booléen Impossible de charger l'image : %s Impossible d'ouvrir le fichier "%s" Devise Date Date et Heure Nombre décimal Souhaitez-vous le remplacer par l'élément courant ? Terminer Nombre en virgule flottante Séparateur des milliers mal placé : %r Séparateur des milliers mal placé Entier Entier long Objet Ouvrir Mot de passe : Remplacer Enregistrer Afficher plus de _détails Chaîne Impossible d'ouvrir le fichier "%s". Permission refusée. Ce champ est obligatoire Ce champ nécessite une date de la forme "%s" et non pas "%s" Ce champ nécessite un nombre et non pas %r Heure Total : Unicode Un séparateur des milliers se trouve dans la partie décimale Il y a plus d'un séparateur décimal ("%s") dans le nombre "%s" _Annuler _Séléctionner _Aujourd'hui 